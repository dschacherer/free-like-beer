terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "us-west-2"
}

# Creates Infrastructure Security group
resource "aws_security_group" "bamboo-node" {
  name = "bamboo-node"
  description = "bamboo Security Group"
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }   
  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }  
  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }    
  ingress {
    from_port = 8085
    to_port = 8085
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }  
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


# Creates Infrastructure EC2 instance with Docker, creates Docker volume, adds aws keypair, and uploads pem file to container to access Prod/Staging nodes
#       sudo mkfs -t ext3 /dev/xvdh
resource "aws_instance" "bamboo-node" {
  ami           = "ami-090717c950a5c34d3"
  instance_type = "t2.medium"
  user_data = <<-EOF
      #!/bin/bash
      set -ex
      sudo apt update -y
      sudo apt install docker -y
      sudo apt install docker.io -y
      sudo usermod -a -G docker ubuntu
      sudo curl -L https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
      sudo chmod +x /usr/local/bin/docker-compose
      echo '{
      "data-root": "/mnt/data-store"
      }' > /etc/docker/daemon.json
      sudo mkdir /mnt/data-store
      sudo mount /dev/xvdh /mnt/data-store
      echo "/dev/xvdh  /mnt/data-store       ext3    defaults      1 2" | sudo tee -a /etc/fstab
      docker volume create --name bambooVolume
      sudo systemctl restart docker
      docker system prune -af
      sleep 10
      docker run --log-driver=splunk --log-opt splunk-token=581b3c8b-d267-447a-b432-9fcd3d1f20f9 --log-opt splunk-url=https://ec2-34-221-45-221.us-west-2.compute.amazonaws.com:8088 --log-opt splunk-insecureskipverify=true --group-add $(getent group docker | cut -d ':' -f 3) -v /var/run/docker.sock:/var/run/docker.sock -v bambooVolume:/var/atlassian/application-data/bamboo --name='bamboo' --init -d -p 54663:54663 -p 8085:8085 jrrickerson/capstone-bamboo
  EOF
  key_name = "jb-test-keypair"
  provisioner "file" {
    source      = "~/Downloads/flb.pem"
    destination = "/tmp/flb.pem"

    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file("~/Downloads/flb.pem")}"
      host        = "${self.public_dns}"
    }
  }
  provisioner "file" {
    source      = "~/Downloads/bamboo.cfg.xml"
    destination = "/tmp/bamboo.cfg.xml"

    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file("~/Downloads/flb.pem")}"
      host        = "${self.public_dns}"
    }
  }
  security_groups = ["${aws_security_group.bamboo-node.name}"]
  tags = {
    Name = "Free-LB-Test-Node"
  }
}

resource "aws_volume_attachment" "ebs_att" {
  device_name = "/dev/xvdh"
  volume_id   = "vol-0d4d5e8648e534ab9"
  instance_id = aws_instance.bamboo-node.id
}

resource "aws_ebs_volume" "example" {
  availability_zone = "us-west-2c"
  size              = 100
}
# Outputs DNS names for (Prod, Staging, Infra) nodes after playbook runs
output "splunk_instance_public_dns" {
  value       = aws_instance.bamboo-node.public_dns
  description = "Splunk node DNS Name"
}

