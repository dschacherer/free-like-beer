terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "us-west-2"
}

# Creates Infrastructure Security group
resource "aws_security_group" "bamboo_node" {
  name = "bamboo_node"
  description = "bamboo Security Group"
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }   
  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }  
  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }    
  ingress {
    from_port = 8085
    to_port = 8085
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }  
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Creates Prod/Staging Security group
resource "aws_security_group" "app-node-test" {
  name = "app-node-test"
  description = "App Security Group"
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }   
  ingress {
    from_port = 8088
    to_port = 8088
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 8000
    to_port = 8000
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }  
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
# Creates Infrastructure EC2 instance with Docker, creates Docker volume, adds aws keypair, and uploads pem file to container to access Prod/Staging nodes
#       sudo mkfs -t ext3 /dev/xvdh
#        WILL BE NEEDED FOR SPLUNK URL -   ${aws_instance.splunk_node.public_dns}
resource "aws_instance" "bamboo_node" {
  ami           = "ami-090717c950a5c34d3"
  availability_zone = "us-west-2c"
  instance_type = "t2.medium"
  user_data = <<-EOF
      #!/bin/bash
      set -ex
      sudo apt update -y
      sudo apt install docker -y
      sudo apt install docker.io -y
      sudo usermod -a -G docker ubuntu
      sudo curl -L https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
      sudo chmod +x /usr/local/bin/docker-compose
      echo '{
      "data-root": "/mnt/data-store"
      }' > /etc/docker/daemon.json
      sudo mkdir /mnt/data-store
      sudo mount /dev/xvdh /mnt/data-store
      echo "/dev/xvdh  /mnt/data-store       ext3    defaults      1 2" | sudo tee -a /etc/fstab
      docker volume create --name bambooVolume
      docker system prune -af
      sudo systemctl restart docker.socket
      sleep 25
      docker run --log-driver=splunk --log-opt splunk-token=581b3c8b-d267-447a-b432-9fcd3d1f20f9 --log-opt splunk-url=https://"${aws_instance.splunk_node.public_dns}":8088 --log-opt splunk-insecureskipverify=true --group-add $(getent group docker | cut -d ':' -f 3) -v /var/run/docker.sock:/var/run/docker.sock -v bambooVolume:/var/atlassian/application-data/bamboo --name='bamboo' --init -d -p 54663:54663 -p 8085:8085 jrrickerson/capstone-bamboo
  EOF
  key_name = "jb-test-keypair"
  provisioner "file" {
    source      = "~/Downloads/flb.pem"
    destination = "/tmp/flb.pem"

    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file("~/Downloads/flb.pem")}"
      host        = "${self.public_dns}"
    }
  }
  provisioner "file" {
    source      = "~/Downloads/bamboo.cfg.xml"
    destination = "/tmp/bamboo.cfg.xml"

    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file("~/Downloads/flb.pem")}"
      host        = "${self.public_dns}"
    }
  }
  security_groups = ["${aws_security_group.bamboo_node.name}"]
  tags = {
    Name = "Free-LB-Bamboo-Node"
  }
}

# Creates Staging EC2 instance with Docker installed and aws keypair associated
resource "aws_instance" "app_staging" {
  ami           = "ami-090717c950a5c34d3"
  instance_type = "t2.micro"
  user_data = <<-EOF
      #!/bin/bash
      set -ex
      sudo apt update -y
      sudo apt install docker -y
      sudo apt install docker.io -y
      sudo usermod -a -G docker ubuntu
      sudo curl -L https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
      sudo chmod +x /usr/local/bin/docker-compose
      echo '{
      "data-root": "/mnt/data-store"
      }' > /etc/docker/daemon.json
      sudo mkdir /mnt/data-store
      sudo mount /dev/xvdh /mnt/data-store
      echo "/dev/xvdh  /mnt/data-store       ext3    defaults      1 2" | sudo tee -a /etc/fstab
      docker volume create --name bambooVolume
      sudo systemctl restart docker
      docker system prune -a -f
      sleep 10
  EOF
  key_name = "jb-test-keypair"
  security_groups = ["${aws_security_group.app-node-test.name}"]
  tags = {
    Name = "Free-LB-Staging-Node"
  }
}

#Creates Prod EC2 instance with Docker installed and aws keypair associated
resource "aws_instance" "app_prod" {
  ami           = "ami-090717c950a5c34d3"
  availability_zone = "us-west-2b"
  instance_type = "t2.micro"
  user_data = <<-EOF
      #!/bin/bash
      set -ex
      sudo apt update -y
      sudo apt install docker -y
      sudo apt install docker.io -y
      sudo usermod -a -G docker ubuntu
      sudo curl -L https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
      sudo chmod +x /usr/local/bin/docker-compose
      echo '{
      "data-root": "/mnt/data-store"
      }' > /etc/docker/daemon.json
      sudo mkdir /mnt/data-store
      sudo mount /dev/xvdh /mnt/data-store
      echo "/dev/xvdh  /mnt/data-store       ext3    defaults      1 2" | sudo tee -a /etc/fstab
      sudo systemctl restart docker
      docker system prune -a -f
      sleep 10
  EOF
  key_name = "jb-test-keypair"
  security_groups = ["${aws_security_group.app-node-test.name}"]
  tags = {
    Name = "Free-LB-Prod-Node"
  }
}
 
# Creates Splunk EC2 instance with Docker, adds aws keypair, and uploads pem file to container to access Prod/Staging nodes
resource "aws_instance" "splunk_node" {
  ami           = "ami-090717c950a5c34d3"
  availability_zone = "us-west-2c"
  instance_type = "t2.medium"
  user_data = <<-EOF
      #!/bin/bash
      set -ex
      sudo apt update -y
      sudo apt install docker -y
      sudo apt install docker.io -y
      sudo usermod -a -G docker ubuntu
      sudo curl -L https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
      sudo chmod +x /usr/local/bin/docker-compose
      echo '{
      "data-root": "/mnt/data-store"
      }' > /etc/docker/daemon.json
      sudo mkdir /mnt/data-store
      sudo mount /dev/xvdh /mnt/data-store
      echo "/dev/xvdh  /mnt/data-store       ext3    defaults      1 2" | sudo tee -a /etc/fstab
      sudo systemctl restart docker
      docker system prune -a -f
      sleep 10
      docker pull splunk/splunk:latest
      docker run -d -p 8000:8000 -p 8088:8088 -v /mnt/data-store/volumes/44af7b12cf9f444273585a4f6cefafdfd5f6866686cd426ccfe4c64572a1c03f/_data:/opt/splunk/etc -v /mnt/data-store/volumes/d955c42ecdbcfe1f8257cfd56a5a71ec53f0a40885c91a2e312667d418ed2b1c/_data:/opt/splunk/var -e "SPLUNK_START_ARGS=--accept-license" -e "SPLUNK_PASSWORD=password1234" --name splunk splunk/splunk:latest
  EOF
  key_name = "jb-test-keypair"
  root_block_device {
    volume_size           = "150"
    delete_on_termination = true
  }
  security_groups = ["${aws_security_group.app-node-test.name}"]
  tags = {
    Name = "Free-LB-Splunk-Node"
  }
}

resource "aws_volume_attachment" "ebs_bamboo_att" {
  device_name = "/dev/xvdh"
  volume_id   = "vol-0d4d5e8648e534ab9"
  instance_id = aws_instance.bamboo_node.id
}

resource "aws_volume_attachment" "ebs_stage_att" {
  device_name = "/dev/xvdh"
  volume_id   = "vol-0c70db11a63d9a53c"
  instance_id = aws_instance.app_staging.id
}

resource "aws_volume_attachment" "ebs_prod_att" {
  device_name = "/dev/xvdh"
  volume_id   = "vol-0a03f009290d0e5ad"
  instance_id = aws_instance.app_prod.id
}

resource "aws_volume_attachment" "ebs_splunk_att" {
  device_name = "/dev/xvdh"
  volume_id   = "vol-08446cf90450f4296"
  instance_id = aws_instance.splunk_node.id
}

# removed from state
# resource "aws_ebs_volume" "example" {
#   availability_zone = "us-west-2b"
#   size              = 100
#   tags = {
#     Name = "Free-LB-Test-Node"
#   }
# }

# Outputs DNS names for (Prod, Staging, Infra) nodes after playbook runs
# output "splunk_instance_public_dns" {
#   value       = aws_instance.splunk_node.public_dns
#   description = "Splunk node DNS Name"
# }

# Builds Ansible hosts file
resource "null_resource" "vars_file" {
  depends_on = [ aws_instance.splunk_node , aws_instance.app_staging , aws_instance.app_prod ]
  provisioner "local-exec" {
    command = "cp hosts.default vars.txt"
  }
  provisioner "local-exec" {
    command = "echo staging=${aws_instance.app_staging.public_dns} >> vars.txt"
  }
  provisioner "local-exec" {
    command = "echo prod=${aws_instance.app_prod.public_dns} >> vars.txt"
  }
  provisioner "local-exec" {
    command = "echo splunk=${aws_instance.splunk_node.public_dns} >> vars.txt"
  }
}

# Builds Ansible hosts file
resource "null_resource" "ansible-provision" {
  depends_on = [ aws_instance.bamboo_node , aws_instance.app_staging , aws_instance.app_prod, aws_instance.splunk_node ]
  provisioner "local-exec" {
    command = "cp hosts.default hosts"
  }
  provisioner "local-exec" {
    command = "echo  >> hosts"
  }
  provisioner "local-exec" {
    command = "echo [infra] >> hosts"
  }
  provisioner "local-exec" {
    command = "echo ${aws_instance.bamboo_node.public_dns} >> hosts"
  }
  provisioner "local-exec" {
    command = "echo  >> hosts"
  }
  provisioner "local-exec" {
    command = "echo [staging] >> hosts"
  }
  provisioner "local-exec" {
    command = "echo ${aws_instance.app_staging.public_dns} >> hosts"
  }
  provisioner "local-exec" {
    command = "echo  >> hosts"
  }
  provisioner "local-exec" {
    command = "echo [prod] >> hosts"
  }
  provisioner "local-exec" {
    command = "echo ${aws_instance.app_prod.public_dns} >> hosts"
  }
  provisioner "local-exec" {
    command = "echo  >> hosts"
  }
  provisioner "local-exec" {
    command = "echo [prod] >> hosts"
  }
  provisioner "local-exec" {
    command = "echo ${aws_instance.splunk_node.public_dns} >> hosts"
  }
  provisioner "local-exec" {
    command = "echo  >> hosts"
  }
  provisioner "local-exec" {
    command = "echo [all:vars] >> hosts"
  }
  provisioner "local-exec" {
    command = "echo ansible_connection=ssh >> hosts"
  }
  provisioner "local-exec" {
    command = "echo ansible_user=ubuntu >> hosts"
  }
  provisioner "local-exec" {
    command = "echo ansible_ssh_private_key_file=/var/atlassian/application-data/bamboo/flb.pem >> hosts"
  }
  provisioner "local-exec" {
    command = "echo  >> hosts"
  }
  provisioner "local-exec" {
    command = "echo [defaults] >> hosts"
  }
  provisioner "local-exec" {
    command = "echo host_key_checking = False >> hosts"
  }
}