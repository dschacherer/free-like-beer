#!/usr/bin/env python3

import requests
import json
import base64
import pprint
import os
import datetime
import argparse
import sys

user = os.environ.get("WORDPRESS_USERNAME")
pythonapp = os.environ.get("WORDPRESS_PASSWORD")
url = os.environ.get("WORDPRESS_URL")


def getparms(args=None):
    """Evaluates arguments submitted with command to determine functionality
    being requested

    Returns:
        args: all inputs provided by user
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "user_input",
        choices=["read", "upload"],
        help="retrieve latest post or create new post",
        type=str,
    )
    parser.add_argument(
        "-f", help="Input file for new post title and content", type=str
    )
    parser.parse_args()
    args = parser.parse_args()
    # print(args)
    # print(sys.stdin.read())
    return vars(args)


def readlatestpost():
    """Retrieves latest wordpress blog post by posted date and prints title and content

    Returns:

    """
    # print(url)
    # print(user)
    # print(pythonapp)
    try:
        r = requests.get(url + "/wp-json/wp/v2/posts")
    except requests.exceptions.ConnectionError:
        print("API Connection error on read!")
        sys.exit(1)
    data = r.json()
    print(r.status_code)
    output = {
        "Title": data[0]["title"]["rendered"],
        "Body": data[0]["content"]["rendered"].replace("\n", ""),
        "Mod_Date": data[0]["modified"],
    }
    return output, r.status_code


def printlatestpost(output):
    print("Title:", output["Title"])
    print("Body:", output["Body"])
    print("Modified Date:", output["Mod_Date"])


def formatpost(filename):

    if filename == "-":
        print("writing for stdin")
        title = sys.stdin.readline()
        content = sys.stdin.read().lstrip()
        #    print(title)
        print(content)
    else:
        with open(filename) as file:
            title = file.readline()
            content = file.read().lstrip()
            print(content)

    now = datetime.datetime.now().replace(microsecond=0)
    # print(now)
    # title = "Latin is a fun language"
    # content = """Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

    # Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
    # """

    post = {
        "date": now,
        "title": title,
        "status": "publish",
        "content": content,
    }

    return post


def writepost(post):
    try:
        r = requests.post(
            url + "/wp-json/wp/v2/posts", auth=(user, pythonapp), data=post
        )
    except requests.exceptions.ConnectionError:
        print("API Connection error on write!")
        sys.exit(1)

    if r.status_code == 401:
        print("Invalid credentials")
        sys.exit(1)
    return r.status_code

    # pprint.pprint(r.content.decode("utf-8"))
    # pprint.pprint("Your post is published on " + str(json.loads(r.content)))


def main():
    args = getparms()
    command = args["user_input"]
    print(command)
    if command == "read":
        output, status_code = readlatestpost()
        printlatestpost(output)
    if command == "upload":
        post = formatpost(args["f"])
        status_code = writepost(post)
    return status_code


if __name__ == "__main__":
    main()
