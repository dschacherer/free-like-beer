#!/usr/bin/env python3

import datetime
import wpapitest
from io import StringIO
from unittest.mock import patch

getreadlatestresponse = [
    {
        "categories": [1],
        "comment_status": "open",
        "content": {"protected": False, "rendered": "<p>UNIT TEST</p>\n"},
        "date": "2021-06-18T14:43:23",
        "date_gmt": "2021-06-18T18:43:23",
        "excerpt": {"protected": False, "rendered": "<p>UNIT TEST</p>\n"},
        "featured_media": 0,
        "format": "standard",
        "guid": {
            "rendered": "http://ec2-34-222-145-112.us-west-2.compute.amazonaws.com:8088/2021/06/18/this-is-a-test-from-docker2134-3/"
        },
        "id": 58,
        "link": "http://ec2-34-222-145-112.us-west-2.compute.amazonaws.com:8088/2021/06/18/this-is-a-test-from-docker2134-3/",
        "meta": [],
        "modified": "2021-06-18T14:43:23",
        "modified_gmt": "2021-06-18T18:43:23",
        "ping_status": "open",
        "slug": "this-is-a-test-from-docker2134-3",
        "status": "publish",
        "sticky": False,
        "tags": [],
        "template": "",
        "title": {"rendered": "THIS IS A TEST FROM DOCKER UNIT TEST"},
        "type": "post",
    }
]

post = {
    "content": "THIS IS A UNIT TEST POST BODY",
    "date": "2021-06-18T12:00:00",
    "status": "publish",
    "title": "THIS IS A UNIT TEST POST\n",
}


def test_getparms():
    with patch("argparse._sys.argv", ["user_input", "read"]):
        args = wpapitest.getparms()
        assert args == {"f": None, "user_input": "read"}


def test_getparms_upload():
    with patch("argparse._sys.argv", ["user_input", "upload", "-f", "-"]):
        args = wpapitest.getparms()
        assert args == {"f": "-", "user_input": "upload"}


def test_readlatestpost():
    with patch("requests.get") as mock_get:
        mock_response = mock_get.return_value
        mock_response.status_code = "200"
        mock_response.json.return_value = getreadlatestresponse
        output, status_code = wpapitest.readlatestpost()

        assert output == {
            "Title": "THIS IS A TEST FROM DOCKER UNIT TEST",
            "Body": "<p>UNIT TEST</p>",
            "Mod_Date": "2021-06-18T14:43:23",
        }
        assert status_code == "200"


def test_formatpost():
    with patch(
        "sys.stdin", StringIO("THIS IS A UNIT TEST POST\nTHIS IS A UNIT TEST POST BODY")
    ):

        output = wpapitest.formatpost("-")

        assert output == {
            "content": "THIS IS A UNIT TEST POST BODY",
            "date": datetime.datetime.now().replace(microsecond=0),
            "status": "publish",
            "title": "THIS IS A UNIT TEST POST\n",
        }


def test_writepost():
    with patch("requests.post") as mock_post:
        mock_response = mock_post.return_value
        mock_response.status_code = "201"
        output = wpapitest.writepost(post)

        assert output == "201"


def test_main_read():
    with patch("argparse._sys.argv", ["user_input", "read"]):
        with patch("requests.get") as mock_get:
            mock_response = mock_get.return_value
            mock_response.status_code = 200
            mock_response.json.return_value = getreadlatestresponse
            return_code = wpapitest.main()
            assert return_code == 200


def test_main_upload():
    with patch(
        "argparse._sys.argv", ["user_input", "upload", "-f", "pyblog/testfile.txt"]
    ):
        with patch("requests.post") as mock_post:
            mock_response = mock_post.return_value
            mock_response.status_code = 201
            return_code = wpapitest.main()
            assert return_code == 201